import os, re, ROOT, sys, pickle, time
from pprint import pprint
from math import *
from array import array
from DataFormats.FWLite import Events, Handle
import numpy as np
from optparse import OptionParser
from auxiliars import *
import multiprocessing

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)

#plots = {'hEffDxy': plots['hEffDxy']}
#"Parser inputs"
pr = OptionParser(usage="%prog [options]")

#Model type, experimental labels
pr.add_option("-d","--datasets" , dest="datasets"   , type="string"      , default="datasets" , help="File to read dataset configuration from")
pr.add_option("-l","--datasetlist"  , dest="datasetlist", type="string"  ,default="Disp_pt30,Both_pt30", help="Datasets to plot, in order to appear in legend")
pr.add_option("-p","--plots"    , dest="plots"      , type="string"      , default="plots"    , help="File to read plot configuration from")
pr.add_option("-q","--plotlist" , dest="plotThis"      , type="string"      , default="yields*"  , help="Plots to be activated")
pr.add_option("--pdir"    , dest="pdir"      , type="string"      , default="."    , help="Where to put the plots into")

pr.add_option("-o","--output"   , dest="output"     , type="string"      , default="out.root"    , help="Output root file")
pr.add_option("-L","--load"     , dest="load"       , type="string"      , default="NONE"        , help="Load histograms from this root file instead of running the whole samples")
pr.add_option("--doPlots"       , dest="doPlots"    , action="store_true", default=False         , help="If activated, also do plots")
pr.add_option("-v","--verbose"  , dest="verbose"    , action="store_true", default=False         , help="If activated, print verbose output")
pr.add_option("-n","--normalize"  , dest="normalize"    , action="store_true", default=False         , help="Normalize histograms to unity")
pr.add_option("-j","--jobs"     , dest="jobs"       , type="int"         , default=-1            , help="Number of parallel jobs to generate. -1 runs as a single thread that should be used for debugging, as otherwise verbosity is deactivated")

(options, args) = pr.parse_args()

## Deal with datasets
datasetsfile = __import__(options.datasets)
datasets = {}
options.datasetlist = (options.datasetlist).split(",")
for key in datasetsfile.datasets:
  if key in options.datasetlist:
    datasets[key] = datasetsfile.datasets[key]
ordereddatasets = options.datasetlist
print ordereddatasets

##Deal with plots

plotsfile = __import__(options.plots)
plots = {}
for key in plotsfile.plots:
  if re.match(options.plotThis,key):
    plots[key] = plotsfile.plots[key]

print plots
if not(os.path.isdir(options.pdir)): 
  os.mkdir(options.pdir)
  os.system("cp ~/www/index.php %s"%options.pdir)

##Deal with plots

def doRun(obj):
  obj.run()
  return 0

class histogramMaker(object):
  def __init__(self, datasets, plots, options = None, nJobs = -1, iJob = -1):
    self.datasets = datasets
    self.plots = plots
    self.verbose = options.verbose
    self.options = options
    self.nJobs = nJobs
    self.iJob = iJob

  def run(self):
    if self.verbose: print "Loading files...."
    self.loadFiles()
    if self.verbose: print "Creating edm handles...."
    self.createHandles()
    if self.verbose: print "Initializating histograms...."
    self.createHistograms()
    if self.options.load == "NONE":
      if self.verbose: print "Looping events...."
      self.insideLoop()
      if self.verbose: print "Saving histograms..."
      self.saveHistograms()

    else:
      if self.verbose: print "Will load events from file %s"%self.options.load
      self.loadHistogramsFromFile()
    if self.options.doPlots and self.nJobs == -1: # Don't let the plotter run if split
      if self.verbose: print "Now producing plots..."
      self.producePlots()

  def runFromPrevs(self, prevs):
    if options.verbose: print "Collecting job contributions..."
    self.loadFiles()
    self.createHistograms()
    self.loadHistogramsFromPrevs(prevs)
    self.saveHistograms()

    if self.options.doPlots:
      self.producePlots()

  def producePlots(self):
    for plot in self.plots:
      p = self.plots[plot]
      canvas = ROOT.TCanvas("c","c", 800,600)
      canvas.cd()
      tleg   = ROOT.TLegend(p["legend"][0], p["legend"][1],p["legend"][2],p["legend"][3])
      tleg.SetTextSize(0.035)
      hists = []
      first=True
      for dset in ordereddatasets:
         hTemp = self.histograms[plot][dset] 
         hTemp.SetLineColor(self.datasets[dset]["color"])
         hTemp.SetMarkerColor(self.datasets[dset]["color"])
         hTemp.SetTitle(";%s;%s"%(p["xlabel"], p["ylabel"]))
         hTemp.GetXaxis().SetRangeUser(p["xrange"][0],p["xrange"][1])
         hTemp.GetYaxis().SetRangeUser(p["yrange"][0],p["yrange"][1])
         if self.options.normalize:
           hTemp.Scale(1./(hTemp.Integral()+0.001))
           hTemp.GetYaxis().SetRangeUser(0,0.4)
         if "xlabels" in p.keys():
           for i, label in enumerate(p["xlabels"]):
             hTemp.GetXaxis().SetBinLabel(i+1, label)
         tleg.AddEntry(hTemp, self.datasets[dset]["label"], 'l')
         hists.append(hTemp)
         if first:
           hTemp.Draw("")
           first=False
         else:
           hTemp.Draw("same")
      tleg.Draw("same")
      canvas.SaveAs(p["savename"].replace("[PDIR]",options.pdir)+".pdf")
      canvas.SaveAs(p["savename"].replace("[PDIR]",options.pdir)+".png")

  def loadFiles(self):
    self.files = {}
    for dataset in self.datasets:
      if type(self.datasets[dataset]["samples"]) != type(["a","b"]): self.files[dataset] = [Events(self.datasets[dataset]["samples"])]
      else: self.files[dataset] = [ Events(i) for i in self.datasets[dataset]["samples"]]

  def createHandles(self):
    self.recohandlesX = {}
    self.recohandlesY = {}

    self.genhandles  = {}
    self.extrahandles= {}
    for dataset in self.datasets:
      self.recohandlesX[dataset] = [Handle(self.datasets[dataset]["recohandleX"]), self.datasets[dataset]["recolabelX"]]
      self.recohandlesY[dataset] = [Handle(self.datasets[dataset]["recohandleY"]), self.datasets[dataset]["recolabelY"]]

      self.genhandles[dataset] = [Handle(self.datasets[dataset]["genhandle"]), self.datasets[dataset]["genlabel"]]
      self.extrahandles[dataset] = []
      for handle,label in zip(self.datasets[dataset]["extrahandles"], self.datasets[dataset]["extralabels"]):
        self.extrahandles[dataset].append([Handle(handle),label])

  def createHistograms(self):
    self.histograms = {}
    for plot in self.plots:
      self.histograms[plot] = {}
      for dataset in self.datasets:
        self.histograms[plot][dataset] = eval(self.plots[plot]["template"].replace("[DATASET]", dataset+ "_" + str(self.iJob) if self.iJob >= 0 else dataset))

  def loadHistogramsFromFile(self):
    theFile = ROOT.TFile(self.options.load, "READ")
    for plot in self.plots:
      for dataset in self.datasets:
        self.histograms[plot][dataset] = theFile.Get(self.histograms[plot][dataset].GetName())

  def loadHistogramsFromPrevs(self,prevs):
    for prev in range(prevs):
      theFile = ROOT.TFile(self.options.output.replace(".root",str(prev)+".root"), "READ")
      for plot in self.plots:
        for dataset in self.datasets:
          #if self.verbose: print "Plot: %s, Dataset: %s"%(plot,dataset) 
          #if self.verbose: print "WARNING: multicore merging yet only implemented for TEfficiencies. You better know what you are doing."
          self.histograms[plot][dataset].Add(theFile.Get(self.histograms[plot][dataset].GetName() + "_" + str(prev)))#prev.histograms[plot][dataset])#prevs[prev][plot][dataset])
      os.system("rm %s"%(self.options.output.replace(".root",str(prev)+".root")))
  def insideLoop(self):
    for f in self.files:
      for ff in self.files[f]:
        if self.verbose: print "..... Dataset: %s"%(f)
        self.loop(f,ff)

  def loop(self,f,ff):
    count = 0
    totalEvents = ff.size()
    for ev in ff:
      # Very lazy parallelization
      count += 1
      if self.nJobs >= 1 and count%self.nJobs != self.iJob : continue 
      #if count >= 20000: continue
      if not count%1000 and self.verbose: print "Dataset %s"%f, count,"/", totalEvents
      ev.getByLabel(self.recohandlesX[f][1], self.recohandlesX[f][0])
      ev.getByLabel(self.recohandlesY[f][1], self.recohandlesY[f][0])

      ev.getByLabel(self.genhandles[f][1], self.genhandles[f][0])
      recoX = self.recohandlesX[f][0].product()
      recoY = self.recohandlesX[f][0].product()

      gens = self.genhandles[f][0].product()
      extra = []
      for ext in self.extrahandles[f]:
        ev.getByLabel(ext[1],ext[0])
        extra.append(ext[0].product())
      for plot in self.plots:
        for igen, gen in enumerate(gens):
            # Eval extra expressions
          for ext in self.datasets[f]["extraexpressions"]: eval(ext)
          genp4 = getP4(gen)
          for rec in recoX:
            recp4X = getP4FromHW(rec)
            if eval(self.plots[plot]["recocutsX"]) and eval(self.datasets[f]["gencuts"]) and eval(self.datasets[f]["recocutsX"]):
               thevarX = eval(self.plots[plot]["thevariableX"])
          for rec in recoY:
            recp4Y = getP4FromHW(rec)
            if eval(self.plots[plot]["recocutsY"]) and eval(self.datasets[f]["gencuts"]) and eval(self.datasets[f]["recocutsY"]):
               thevarY = eval(self.plots[plot]["thevariableY"])

                 



    #for plot in self.plots:
    # self.histograms[plot][f].Print("all")
  def saveHistograms(self):
    outFile = ROOT.TFile(options.output.replace(".root",str(self.iJob) + ".root" if self.iJob >= 0 else ".root"),"RECREATE")
    outFile.cd()
    for plot in self.plots:
      for dataset in self.datasets:
        self.histograms[plot][dataset].Write()
    outFile.Close()



if options.jobs == -1:
  hM = histogramMaker(datasets, plots, options)
  hM.run()

else:
  # First step runs 1/nJobs part of each sample
  hMArray = [ histogramMaker(datasets, plots, options, options.jobs, iJ) for iJ in range(options.jobs)]
  processes = []
  for i in range(options.jobs):
    processes.append(multiprocessing.Process(target=doRun, args=[hMArray[i]]))
    processes[-1].start()
  for i in range(options.jobs):
    processes[i].join()

  hMPlot = histogramMaker(datasets, plots, options)
  hMPlot.runFromPrevs(options.jobs)
  print "We finished!"

