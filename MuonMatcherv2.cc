#include "L1Trigger/L1TMuonOverlapPhase1/plugins/MuonMatcherv2.h"

MuonMatcherv2::MuonMatcherv2(const edm::ParameterSet& iCfg) {
    src_ = iCfg.getParameter<edm::InputTag>("src");
    genParts_token = consumes<std::vector<reco::GenParticle>>(src_);
    produces<std::vector<float>>("propRfirst").setBranchAlias("propRfirst");
    produces<std::vector<float>>("propEtafirst").setBranchAlias("propEtafirst");
    produces<std::vector<float>>("propPhifirst").setBranchAlias("propPhifirst");
    produces<std::vector<float>>("propRsecond").setBranchAlias("propRsecond");
    produces<std::vector<float>>("propEtasecond").setBranchAlias("propEtasecond");
    produces<std::vector<float>>("propPhisecond").setBranchAlias("propPhisecond");
}

MuonMatcherv2::~MuonMatcherv2(){
}

void MuonMatcherv2::beginJob(){
}

void MuonMatcherv2::endJob(){
}

void MuonMatcherv2::produce(edm::Event& iEv, const edm::EventSetup& eventSetup){
  eventSetup.get<GlobalTrackingGeometryRecord>().get(globalGeometry);
  eventSetup.get<IdealMagneticFieldRecord>().get(magField);
  eventSetup.get<TrackingComponentsRecord>().get("SteppingHelixPropagatorAlong", propagator);
  
  std::unique_ptr<std::vector<float>> valuesRfirst(new std::vector<float>);
  std::unique_ptr<std::vector<float>> valuesEtafirst(new std::vector<float>);
  std::unique_ptr<std::vector<float>> valuesPhifirst(new std::vector<float>);
  std::unique_ptr<std::vector<float>> valuesRsecond(new std::vector<float>);
  std::unique_ptr<std::vector<float>> valuesEtasecond(new std::vector<float>);
  std::unique_ptr<std::vector<float>> valuesPhisecond(new std::vector<float>);

  edm::Handle<std::vector<reco::GenParticle>> genParticles;  

  iEv.getByToken(genParts_token, genParticles);
 
  float dRVal = 5;
  int   nDist = 160;
  int iGen = 0;
  for(std::vector<reco::GenParticle>::const_iterator genPart = genParticles->begin() ; genPart != genParticles->end() ; ++genPart){
    iGen = 0;
    for(int iDist = 0; iDist <= nDist ; iDist++){
      //std::cout << "Next iGen: " << iGen << std::endl;
      float theDist = iDist*dRVal;
      float theEta = propagateGenPart(genPart,theDist);
      float thePhi = propagateGenPartPhi(genPart,theDist);
      if(iGen == 0){
        valuesRfirst->push_back(theDist);
        valuesEtafirst->push_back(theEta);
        valuesPhifirst->push_back(thePhi);
        //std::cout << theDist << " , " << theEta << " , " << thePhi << std::endl;
      } 
      else if (iGen == 1){
        valuesRsecond->push_back(theDist);
        valuesEtasecond->push_back(theEta);
        valuesPhisecond->push_back(thePhi);
        //std::cout << theDist << " , " << theEta << " , " << thePhi << std::endl;
      }
    }
    iGen++;
  }

  iEv.put(std::move(valuesRfirst),"propRfirst");//,"genParticlepropR");
  iEv.put(std::move(valuesEtafirst),"propEtafirst");//,"genParticlepropEta");
  iEv.put(std::move(valuesPhifirst),"propPhifirst");//,"genParticlepropPhi");
  iEv.put(std::move(valuesRsecond),"propRsecond");//,"genParticlepropR");
  iEv.put(std::move(valuesEtasecond),"propEtasecond");//,"genParticlepropEta");
  iEv.put(std::move(valuesPhisecond),"propPhisecond");//,"genParticlepropPhi");
}

float MuonMatcherv2::propagateGenPart(std::vector<reco::GenParticle>::const_iterator gP, float theRDist){
  int charge = gP->charge();
  GlobalPoint r3GV(gP->vx(), gP->vy(), gP->vz());
  GlobalVector p3GV(gP->px(), gP->py(), gP->pz());
  GlobalTrajectoryParameters tPars(r3GV, p3GV, charge, &*magField);
  FreeTrajectoryState fts = FreeTrajectoryState(tPars); 
  ReferenceCountingPointer<Surface> rpc;
  rpc = ReferenceCountingPointer<Surface>(new  BoundCylinder( GlobalPoint(0.,0.,0.), TkRotation<float>(), SimpleCylinderBounds( theRDist, theRDist, -1500, 1500 ) ) );
  TrajectoryStateOnSurface trackAtRPC = propagator->propagate(fts, *rpc);
  if (!trackAtRPC.isValid()) return -999; //Something broke when propagating
  else return trackAtRPC.globalPosition().eta();
}


float MuonMatcherv2::propagateGenPartPhi(std::vector<reco::GenParticle>::const_iterator gP, float theRDist){
  int charge = gP->charge();
  GlobalPoint r3GV(gP->vx(), gP->vy(), gP->vz());
  GlobalVector p3GV(gP->px(), gP->py(), gP->pz());
  GlobalTrajectoryParameters tPars(r3GV, p3GV, charge, &*magField);
  FreeTrajectoryState fts = FreeTrajectoryState(tPars);
  ReferenceCountingPointer<Surface> rpc;
  rpc = ReferenceCountingPointer<Surface>(new  BoundCylinder( GlobalPoint(0.,0.,0.), TkRotation<float>(), SimpleCylinderBounds( theRDist, theRDist, -1500, 1500 ) ) );
  TrajectoryStateOnSurface trackAtRPC = propagator->propagate(fts, *rpc);
  if (!trackAtRPC.isValid()) return -999; //Something broke when propagating
  else return trackAtRPC.globalPosition().phi();
}

DEFINE_FWK_MODULE(MuonMatcherv2);
