import os, re, ROOT, sys, pickle, time
from pprint import pprint
from math import *
from array import array
from DataFormats.FWLite import Events, Handle
import numpy as np

pickleName = 'test_withXML.pickle'

def getEta(etaHW):
    return  etaHW/240.*2.61

def getPt(ptHW):
    return max((ptHW-1.)/2., 1)

def modulo(val):
    while val > 2*pi: val -= 2*pi
    while val <0: val += 2*pi
    return val

def getPhi(obj):
    return modulo(((15.+obj.processor()*60.)/360. + obj.hwPhi()/576.)*2*pi)

def getP4(obj):
    pt = obj.pt()
    eta= obj.eta()
    phi= obj.phi()
    v = ROOT.TLorentzVector()
    v.SetPtEtaPhiM( pt, eta, phi, 0)
    return v

def getP4FromHW(obj):
    pt = obj.hwPt()
    eta= obj.hwEta()
    phi= obj.hwPhi()
    v = ROOT.TLorentzVector()
    v.SetPtEtaPhiM( getPt(pt), getEta(eta), getPhi(obj), 0)
    return v


def getDisplacementFromPtHW(obj):
   #print obj.hwPt(), type(obj.hwPt())
   if obj.hwPt() < 999: dxy = 0
   else: dxy= (obj.hwPt()%10000)
   sign = 1.
   if obj.hwPt() >= 20000: sign = -1.#print dxy, obj.hwPt()
   return dxy*sign





muonHandle, muonLabel = Handle("BXVector<l1t::RegionalMuonCand>"), ("simBayesOmtfDigisDisplaced", "OMTF", "L1TMuonEmulation" )
genHandle, genLabel = Handle("vector<reco::GenParticle>"), "genParticles"
newEtaHandle, newEtaLabel = Handle("edm::ValueMap<float>"), ( "dispGenEta","genParticledispEta","MuonMatcher")
newPhiHandle, newPhiLabel = Handle("edm::ValueMap<float>"), ( "dispGenEta","genParticledispPhi","MuonMatcher")
mu1TrackRHandle, mu1TrackRLabel = Handle("vector<float>"), ( "dispGenEta","propRfirst","MuonTracker")
mu1TrackEtaHandle, mu1TrackEtaLabel = Handle("vector<float>"), ( "dispGenEta","propEtafirst","MuonTracker")
mu1TrackPhiHandle, mu1TrackPhiLabel = Handle("vector<float>"), ( "dispGenEta","propPhifirst","MuonTracker")
mu2TrackRHandle, mu2TrackRLabel = Handle("vector<float>"), ( "dispGenEta","propRsecond","MuonTracker")
mu2TrackEtaHandle, mu2TrackEtaLabel = Handle("vector<float>"), ( "dispGenEta","propEtasecond","MuonTracker")
mu2TrackPhiHandle, mu2TrackPhiLabel = Handle("vector<float>"), ( "dispGenEta","propPhisecond","MuonTracker")




maxEvents = -1

for i in [1]:
    outputDict = {}
    print 'starting to process'
    events = Events("l1tomtf_superprimitives1_withgen.root")
    #events = Events("/eos/cms//store/group/phys_muon/sesanche/overlap_oct16/Mu_FlatPt2to100-pythia8-gun/SingleMu_NOPU.root")
    print 'we got the events'
    count = 0
    toDump = {}
    for ev in events:
        #print "Event!"
        if not count%1000:  print count, events.size()
        count = count + 1
        if count == 50000: break
        #if count == 1000: break
        if count > maxEvents and maxEvents > 0: break
        ev.getByLabel(muonLabel, muonHandle)
        ev.getByLabel(genLabel, genHandle)
        ev.getByLabel(newEtaLabel, newEtaHandle)
        ev.getByLabel(newPhiLabel, newPhiHandle)
        ev.getByLabel(mu1TrackRLabel, mu1TrackRHandle)
        ev.getByLabel(mu1TrackPhiLabel, mu1TrackPhiHandle)
        ev.getByLabel(mu1TrackEtaLabel, mu1TrackEtaHandle)
        ev.getByLabel(mu2TrackRLabel, mu2TrackRHandle)
        ev.getByLabel(mu2TrackPhiLabel, mu2TrackPhiHandle)
        ev.getByLabel(mu2TrackEtaLabel, mu2TrackEtaHandle)

        muons = muonHandle.product()
        gens  = genHandle.product()
        newEta = newEtaHandle.product()
        newPhi = newPhiHandle.product()
        mu1R, mu1Eta, mu1Phi = mu1TrackRHandle.product(), mu1TrackEtaHandle.product(), mu1TrackPhiHandle.product()
        mu2R, mu2Eta, mu2Phi = mu2TrackRHandle.product(), mu2TrackEtaHandle.product(), mu2TrackPhiHandle.product()


        goodmuons = []
        goodIndex = 0
        for bxNumber in range(muons.getFirstBX(), muons.getLastBX()+1):
           
            size = muons.size(bxNumber)
            for i in range(size):
                muon = muons[i+goodIndex]
                #if muon.trackFinderType() not in [1,2]: continue
                #if muon.hwQual() < 12: continue

                goodmuons.append( muon)
                #print "RECO:", getP4FromHW(muon).Pt()

            goodIndex = goodIndex + size
        #print "Next event!"
        ig = 0
        for gen in gens:
            newgen = {}
            if abs(gen.pdgId()) != 13: continue
            newgen["pt"]   = gen.pt()
            newgen["phi"]  = gen.phi()
            newgen["eta"]  = gen.eta()
            newgen["mass"] = gen.mass()
            newp4 = ROOT.Math.LorentzVector("ROOT::Math::PtEtaPhiM4D<double>")(gen.pt(), newEta.get(ig), newPhi.get(ig), gen.mass())
            ig += 1 
            gen.setP4(newp4)
            #print "GEN:", getP4(gen).Eta(), gen.eta()
            matched = False
            v_gen = getP4(gen)
            dxy_gen = abs((-1*gen.vx()* v_gen.Py() + gen.vy()* v_gen.Px()) / v_gen.Pt())
            newgen["vx"]  = gen.vx()
            newgen["vy"]  = gen.vy()
            newgen["dxy"] = dxy_gen 
            newgen["vz"]  = gen.vz()

            newgen["L1dxy"] = -999

            for mu in goodmuons:
                v_mu = getP4FromHW(mu)
                if v_mu.DeltaR( v_gen ) < 0.5 and mu.hwQual() >= 12:
                  matched = True 
                  newgen["L1dxy"] = getDisplacementFromPtHW(mu)

            newgen["matched"] = matched
            if ig == 1:
              iStart = 0
              for i, et in enumerate(mu1Eta):
                if et != -999:
                  iStart = i
                  break

              newgen["Rtrack" ] = [float(f) for f in mu1R[iStart:160]]
              newgen["Reta" ] = [float(f) for f in mu1Eta[iStart:160]]
              newgen["Rz"   ] = []
              for i in range(len(newgen["Reta"])):
                #print len(newgen["Reta"]), len(mu1Eta),i, newgen["Rtrack" ][i], newgen["Reta" ][i]
                newgen["Rz"   ].append(newgen["Rtrack" ][i]*sinh(newgen["Reta" ][i]))
              newgen["Rphi" ] = [float(f) for f in mu1Phi[iStart:]]

            """if ig == 2:
              iStart = 0
              for i, et in enumerate(mu1Eta):
                if et != -999:
                  iStart = i
                  break

              newgen["Rtrack" ] = [float(f) for f in mu2R[iStart:]]
              newgen["Reta" ] = [float(f) for f in mu2Eta[iStart:]]
              newgen["Rz"   ] = [newgen["Rtrack" ][i]*sinh(newgen["Reta" ][i]) for i in range(len(newgen["Reta" ]))]
              newgen["Rphi" ] = [float(f) for f in mu2Phi[iStart:]]"""

            toDump[len(toDump.keys())] = newgen
    #print toDump
    import pickle
    with open(pickleName, 'wb') as handle:
       pickle.dump(toDump, handle, protocol=pickle.HIGHEST_PROTOCOL)
