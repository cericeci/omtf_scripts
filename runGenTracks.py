import FWCore.ParameterSet.Config as cms
process = cms.Process("MuonTracker")

process.load("FWCore.MessageLogger.MessageLogger_cfi")

process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(1)
process.options = cms.untracked.PSet(wantSummary = cms.untracked.bool(False))
process.source = cms.Source('PoolSource',
 fileNames = cms.untracked.vstring('file:///pool/phedex/userstorage/carlosec/omtf/PhaseIITDRSpring19DR_Mu_FlatPt2to100_noPU_v31_E0D5C6A5-B855-D14F-9124-0B2C9B28D0EA_dump4000Ev.root',
 )                           
)
	                    
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100))
# PostLS1 geometry used
process.load('Configuration.Geometry.GeometryExtended2015Reco_cff')
process.load('Configuration.Geometry.GeometryExtended2015_cff')

process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.MagneticField_cff')

############################
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '103X_upgrade2023_realistic_v2', '')

process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi")

####Event Setup Producer
process.load("L1Trigger.L1TMuonOverlapPhase1.MuonMatcherv2_cfg")

process.dispSeq = cms.Sequence( process.dispGenEta 
)

process.dispPath = cms.Path(process.dispSeq)

process.out = cms.OutputModule("PoolOutputModule", 
   fileName = cms.untracked.string("genTracksOnly.root")
)

process.options.numberOfThreads=cms.untracked.uint32(8)
process.options.numberOfStreams=cms.untracked.uint32(0)


process.output_step = cms.EndPath(process.out)
process.schedule = cms.Schedule(process.dispPath)
process.schedule.extend([process.output_step])
