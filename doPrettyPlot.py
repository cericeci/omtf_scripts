import ROOT
import pickle
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg 
from math import sin, cos

dump = pickle.load(open("test_withXML.pickle","r"))

img  = mpimg.imread("cms_notext.png")
img2 = mpimg.imread("cmsdtxy.png")
props = dict(boxstyle='round', facecolor='wheat')
#plt.imshow(img,extent=[0,1200.,0.,800.], interpolation='none')
#plt.imshow(img,extent=[0,-1200.,0.,800.], interpolation='none')
#plt.imshow(img,extent=[0,1200.,0.,-800.], interpolation='none')
#plt.imshow(img,extent=[0,-1200.,0.,-800.], interpolation='none')

iTot = 0
iMatch = 0
for j in range(1000):
  print "Plot:", j
  i = j*2
  plt.axis([0,1200,0,800])
  plt.xlabel("z [cm]")
  plt.ylabel("R [cm]")
  print dump[i]["matched"]
  for k, R in enumerate(dump[i]["Rtrack"]):
    if R == 500:
      etaat500 = dump[i]["Reta"][k]

  if True: #abs(etaat500) >= 0.82 and abs(etaat500) <= 1.24:
    iTot += 1
    plt.imshow(img,extent=[0,1200.,0.,800.], interpolation='none')
    if not(dump[i]["matched"]):
      plt.plot(abs(dump[i]["vz"]), (dump[i]["vx"]**2+dump[i]["vy"]**2)**0.5,"rx", linewidth=1)
      plt.plot([abs(k) for k in dump[i]["Rz"]], dump[i]["Rtrack"],"r--", linewidth=3)
      plt.text(50, 750, '\n'.join([r'$p_{T}=%1.3f$'%dump[i]["pt"],r'$d_{xy}^{gen}=%1.3f$'%dump[i]["dxy"],r'$d_{xy}^{L1}=%1.3f$'%abs(dump[i]["L1dxy"]) ]), fontsize=14,
        verticalalignment='top', bbox=props)
      plt.xlabel(r"$|z|$ [cm]")
      plt.ylabel(r"$R_{T}$ [cm]")
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_rz_bad.pdf"%j)
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_rz_bad.png"%j)
      plt.clf()

    else:
      iMatch += 1
      plt.plot(abs(dump[i]["vz"]), (dump[i]["vx"]**2+dump[i]["vy"]**2)**0.5,"gx", linewidth=1)
      plt.plot([abs(k) for k in dump[i]["Rz"]], dump[i]["Rtrack"],"g--", linewidth=3)

      plt.text(50, 750, '\n'.join([r'$p_{T}=%1.3f$'%dump[i]["pt"],r'$d_{xy}^{gen}=%1.3f$'%dump[i]["dxy"],r'$d_{xy}^{L1}=%1.3f$'%abs(dump[i]["L1dxy"]) ]), fontsize=14,
        verticalalignment='top', bbox=props)
      plt.xlabel(r"$|z|$ [cm]")
      plt.ylabel(r"$R_{T}$ [cm]")

      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_rz_good.pdf"%j)
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_rz_good.png"%j)
      plt.clf()
    
  if True: #abs(etaat500) >= 0.82 and abs(etaat500) <= 1.24:
    iTot += 1
    plt.imshow(img2,extent=[-800.,800.,-770,770], interpolation='none')
    if not(dump[i]["matched"]):
      plt.plot(dump[i]["vx"], dump[i]["vy"],"rx", linewidth=1)
      plt.plot([dump[i]["Rtrack"][k]*cos(dump[i]["Rphi"][k]) for k in range(len(dump[i]["Rtrack"]))], [dump[i]["Rtrack"][k]*sin(dump[i]["Rphi"][k]) for k in range(len(dump[i]["Rtrack"]))],"r--", linewidth=3)
      plt.xlabel(r"$x$ [cm]")
      plt.ylabel(r"$y$ [cm]")

      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_xy_bad.pdf"%j)
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_xy_bad.png"%j)
      plt.clf()
    else:
      plt.plot(dump[i]["vx"], dump[i]["vy"],"gx", linewidth=1)
      plt.plot([dump[i]["Rtrack"][k]*cos(dump[i]["Rphi"][k]) for k in range(len(dump[i]["Rtrack"]))], [dump[i]["Rtrack"][k]*sin(dump[i]["Rphi"][k]) for k in range(len(dump[i]["Rtrack"]))],"g--", linewidth=3)
      plt.xlabel(r"$x$ [cm]")
      plt.ylabel(r"$y$ [cm]")
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_xy_good.pdf"%j)
      plt.savefig("/nfs/fanae/user/carlosec/www/public/omtf_2020/single_events_v2/event%i_xy_good.png"%j)
      plt.clf()

print iTot, iMatch
#plt.savefig("/nfs/fanae/user/carlosec/www/public/testcms.pdf")
