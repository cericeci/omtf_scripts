import ROOT
from math import *


def getEta(etaHW):
    return  etaHW/240.*2.61

def getPt(ptHW):
    return max((ptHW-1.)/2., 1)

def modulo(val):
    while val > 2*pi: val -= 2*pi
    while val <0: val += 2*pi
    return val

def getPhi(obj):
    return modulo(((15.+obj.processor()*60.)/360. + obj.hwPhi()/576.)*2*pi)

def getP4(obj):
    pt = obj.pt()
    eta= obj.eta()
    phi= obj.phi()
    v = ROOT.TLorentzVector()
    v.SetPtEtaPhiM( pt, eta, phi, 0)
    return v

def getP4FromHW(obj):
    pt = obj.hwPt()
    eta= obj.hwEta()
    phi= obj.hwPhi()
    v = ROOT.TLorentzVector()
    v.SetPtEtaPhiM( getPt(pt), getEta(eta), getPhi(obj), 0)
    return v

def getDisplacementFromPtHW(obj):
   #print obj.hwPt(), type(obj.hwPt())
   if obj.hwPt() < 999: dxy = 0
   else: dxy= (obj.hwPt()%10000)
   sign = 1.
   if obj.hwPt() >= 20000: sign = -1.#print dxy, obj.hwPt()
   #print dxy, sign
   return dxy*sign

def getNFiredLayers(obj):
    trckAdr = obj.trackSubAddress(0)
    bitwise = bin(trckAdr)[2:]
    iLayer = 0
    for f in bitwise:
      if f=="1":
        iLayer +=1
    return iLayer

def getPDF(obj):
    pdf = obj.trackSubAddress(2)
    return pdf    

def getPDF_preferprompt(obj):
    pdf =  obj.trackSubAddress(2)
    if getP4FromHW(obj).Pt() < 500 or abs(getDisplacementFromPtHW(obj)) >= 50: pdf = pdf*100000000
    return pdf

def getFiredLayers(obj):
    trckAdr = obj.trackSubAddress(0)
    bitwise = bin(trckAdr)[2:]
    layers = []
    for i, f in enumerate(bitwise):
      if int(f) == 1: layers.append(i)
    #iLayers = []
    #for f in bitwise:
    #  iLayers.append(int(f))
    #iLayers = iLayers[::-1]
    return layers

def redefinequality(obj):
    if getP4FromHW(obj).Pt() < 4500: return obj.hwQual() #Only for displaced
    c =  bin(obj.trackAddress()[0])[2:]
    while len(c) < 18:
      c = "0"+c 
    #if c == "001000110000000011" or c=="000000011000000011" or c=="000011110000000011" or c=="000011100000001100" or c=="000000111000000011" or c=="000000001000000011" or c=="000011100000000011" or c=="000000101000000011" or c=="000011010000000011" or c=="000000111000000000":
    #if c in ['000010010000000011', '000001000001001100', '000010100000001111', '001011100001001100', '010000011010000011', '000000110100000011', '000001110001000011', '001011110000001100', '001000110001001111', '001010000001000000', '001001000000001100', '000011110000001100', '000111110001001111', '001011010001001111', '001011110001001100', '000111110001111111', '011001110010001111', '001011000000001100', '010000011010000000', '000000110001000011', '000011000000001111', '000010010000001100', '000000011010000011', '010001110011001111', '001000010001000011', '011000000011000000', '100000011100000000', '110000010010000011', '000010100000000011', '000111100000001111', '010000110000000011', '000110100000111100', '001111110001001111', '000000101100000000', '000011010000001111', '000001010000000011', '000000010010000011', '000000011100000000', '001011000001001100', '000010100000001100', '000010110000001111', '000101100000111100', '001001000001001100', '000011010000001100', '110000011110000011', '001001110001001111', '000011010000000011', '110000011110000000', '001001110001000011', '011001110011001111', '000101000000001100', '000000111000000000', '000001100000000011', '011011110001001111', '000010110000000011', '001000110001000011', '001000100000000011', '000001100000001100', '010000010000000011', '000011100000001111', '000111100000111100', '000000101000000011', '010000010010000011', '110000000110000000', '000011100000000011', '000111100000111111', '000000001000000011', '001011110001001111', '000011110000001111', '000000111000000011', '000011100000001100', '110000010110000011', '001000010000000011', '000011110000000011', '000000011000000011', '001000110000000011']:
    #if c in ['000010010000000011', '000001000001001100', '000010100000001111', '001011100001001100', '010000011010000011', '000000110100000011', '000001110001000011', '001011110000001100', '001000110001001111', '001010000001000000', '001001000000001100', '000011110000001100', '000111100000001100', '000111110001001111', '001011010001001111', '001011110001001100', '000111110001111111', '011001110010001111', '001011000000001100', '010000011010000000', '000000110001000011', '000011000000001111', '000010010000001100', '000000011010000011', '010001110011001111', '001000010001000011', '011000000011000000', '100000011100000000', '110000010010000011', '000010100000000011', '000111100000001111', '010000110000000011', '000110100000111100', '001111110001001111', '000000101100000000', '000011010000001111', '000001010000000011', '000000010010000011', '000000011100000000', '001011000001001100', '000010100000001100', '000010110000001111', '000101100000111100', '001001000001001100', '000011010000001100', '110000011110000011', '001001110001001111', '000011010000000011', '110000011110000000', '001001110001000011', '011001110011001111', '000101000000001100', '000000111000000000', '000001100000000011', '011011110001001111', '000010110000000011', '001000110001000011', '001000100000000011', '000001100000001100', '010000010000000011', '000011100000001111', '000111100000111100', '000000101000000011', '010000010010000011', '110000000110000000', '000011100000000011', '000111100000111111', '000000001000000011', '001011110001001111', '000011110000001111', '000000111000000011', '000101000000111100', '000011100000001100', '110000010110000011', '001000010000000011', '000011110000000011', '000000011000000011', '001000110000000011']:
      #print "Demoting pattern with hits %s"%c
   #   return 4
    return obj.hwQual() 

def weightPattern_unif(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 10: return 1
    if dxy < 20: return 1
    if dxy < 30: return 1
    if dxy < 40: return 1
    if dxy < 50: return 1
    if dxy < 60: return 1
    if dxy < 80: return 1
    if dxy < 100: return 1
    if dxy < 150: return 1
    if dxy < 200: return 1
    if dxy >= 200: return 1

def weightPattern_choosePrompt(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    if getP4FromHW(l1).Pt() >= 500: return 0

def weightPattern_absurd(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 10: return 100
    if dxy < 20: return 100
    if dxy < 30: return 100
    if dxy < 40: return 100
    if dxy < 50: return 100
    if dxy < 60: return 100
    if dxy < 80: return 100
    if dxy < 100: return 100
    if dxy < 150: return 100
    if dxy < 200: return 100
    if dxy >= 200: return 100


def weightPattern_optim(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 10: return 1/1.1
    if dxy < 20: return 1/1.1
    if dxy < 30: return 1/1.2
    if dxy < 40: return 1/1.2
    if dxy < 50: return 1/1.2
    if dxy < 60: return 1/1.4
    if dxy < 80: return 1/1.5
    if dxy < 100: return 1/1.5
    if dxy < 150: return 1/1.5
    if dxy < 200: return 1/1.6
    if dxy >= 200: return 1/1.6

def weightPattern_optim_v2(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 10: return 1/1.4
    if dxy < 20: return 1/1.4
    if dxy < 30: return 1/1.4
    if dxy < 40: return 1/1.4
    if dxy < 50: return 1/1.4
    if dxy < 60: return 1/1.4
    if dxy < 80: return 1/1.5
    if dxy < 100: return 1/1.5
    if dxy < 150: return 1/1.5
    if dxy < 200: return 1/1.6
    if dxy >= 200: return 1/1.6

def weightPattern_optim_v3(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    #return 1/100000000000.
    if dxy < 30: return 1/1.5
    if dxy < 60: return 1/1.5
    if dxy < 80: return 1/1.5
    if dxy < 100: return 1/1.5
    if dxy < 120: return 1/1.5
    if dxy < 140: return 1/1.5
    if dxy < 160: return 1/1.5
    if dxy < 180: return 1/1.5
    if dxy < 200: return 1/1.5
    if dxy < 250: return 1/1.5
    if dxy >= 250: return 1/1.5


def weightPattern_optim_v4(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/10000000
    if dxy < 60: return 1/10000000
    if dxy < 80: return 1/10000000
    if dxy < 100: return 1/1000000
    if dxy < 120: return 1/1000000
    if dxy < 140: return 1/1000000
    if dxy < 160: return 1/1000000
    if dxy < 180: return 1/1000000
    if dxy < 200: return 1/1000000.
    if dxy < 250: return 1/1000000.
    if dxy >= 250: return 1/1000000.




def weightPattern_optim_101(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.1
    if dxy < 60: return 1/1.1
    if dxy < 80: return 1/1.1
    if dxy < 100: return 1/1.1
    if dxy < 120: return 1/1.1
    if dxy < 140: return 1/1.1
    if dxy < 160: return 1/1.1
    if dxy < 180: return 1/1.1
    if dxy < 200: return 1/1.1
    if dxy < 250: return 1/1.1
    if dxy >= 250: return 1/1.1


def weightPattern_optim_102(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.2
    if dxy < 60: return 1/1.2
    if dxy < 80: return 1/1.2
    if dxy < 100: return 1/1.2
    if dxy < 120: return 1/1.2
    if dxy < 140: return 1/1.2
    if dxy < 160: return 1/1.2
    if dxy < 180: return 1/1.2
    if dxy < 200: return 1/1.2
    if dxy < 250: return 1/1.2
    if dxy >= 250: return 1/1.2


def weightPattern_optim_103(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.3
    if dxy < 60: return 1/1.3
    if dxy < 80: return 1/1.3
    if dxy < 100: return 1/1.3
    if dxy < 120: return 1/1.3
    if dxy < 140: return 1/1.3
    if dxy < 160: return 1/1.3
    if dxy < 180: return 1/1.3
    if dxy < 200: return 1/1.3
    if dxy < 250: return 1/1.3
    if dxy >= 250: return 1/1.3


def weightPattern_optim_104(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.4
    if dxy < 60: return 1/1.4
    if dxy < 80: return 1/1.4
    if dxy < 100: return 1/1.4
    if dxy < 120: return 1/1.4
    if dxy < 140: return 1/1.4
    if dxy < 160: return 1/1.4
    if dxy < 180: return 1/1.4
    if dxy < 200: return 1/1.4
    if dxy < 250: return 1/1.4
    if dxy >= 250: return 1/1.4

def weightPattern_optim_105(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.5
    if dxy < 60: return 1/1.5
    if dxy < 80: return 1/1.5
    if dxy < 100: return 1/1.5
    if dxy < 120: return 1/1.5
    if dxy < 140: return 1/1.5
    if dxy < 160: return 1/1.5
    if dxy < 180: return 1/1.5
    if dxy < 200: return 1/1.5
    if dxy < 250: return 1/1.5
    if dxy >= 250: return 1/1.5

def weightPattern_optim_106(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.6
    if dxy < 60: return 1/1.6
    if dxy < 80: return 1/1.6
    if dxy < 100: return 1/1.6
    if dxy < 120: return 1/1.6
    if dxy < 140: return 1/1.6
    if dxy < 160: return 1/1.6
    if dxy < 180: return 1/1.6
    if dxy < 200: return 1/1.6
    if dxy < 250: return 1/1.6
    if dxy >= 250: return 1/1.6

def weightPattern_optim_107(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.7
    if dxy < 60: return 1/1.7
    if dxy < 80: return 1/1.7
    if dxy < 100: return 1/1.7
    if dxy < 120: return 1/1.7
    if dxy < 140: return 1/1.7
    if dxy < 160: return 1/1.7
    if dxy < 180: return 1/1.7
    if dxy < 200: return 1/1.7
    if dxy < 250: return 1/1.7
    if dxy >= 250: return 1/1.7


def weightPattern_optim_108(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.8
    if dxy < 60: return 1/1.8
    if dxy < 80: return 1/1.8
    if dxy < 100: return 1/1.8
    if dxy < 120: return 1/1.8
    if dxy < 140: return 1/1.8
    if dxy < 160: return 1/1.8
    if dxy < 180: return 1/1.8
    if dxy < 200: return 1/1.8
    if dxy < 250: return 1/1.8
    if dxy >= 250: return 1/1.8

def weightPattern_optim_109(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/1.9
    if dxy < 60: return 1/1.9
    if dxy < 80: return 1/1.9
    if dxy < 100: return 1/1.9
    if dxy < 120: return 1/1.9
    if dxy < 140: return 1/1.9
    if dxy < 160: return 1/1.9
    if dxy < 180: return 1/1.9
    if dxy < 200: return 1/1.9
    if dxy < 250: return 1/1.9
    if dxy >= 250: return 1/1.9

def weightPattern_optim_1010(l1):
    if getP4FromHW(l1).Pt() < 500: return 1
    dxy = abs(getDisplacementFromPtHW(l1))
    if dxy < 30: return 1/2.
    if dxy < 60: return 1/2.
    if dxy < 80: return 1/2.
    if dxy < 100: return 1/2.
    if dxy < 120: return 1/2.
    if dxy < 140: return 1/2.
    if dxy < 160: return 1/2.
    if dxy < 180: return 1/2.
    if dxy < 200: return 1/2.
    if dxy < 250: return 1/2,
    if dxy >= 250: return 1/2.
