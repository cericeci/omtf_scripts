import ROOT
import copy

rateptbins = [0.5, 1.5, 2.5, 3.5, 4.25, 4.75, 5.5, 6.5, 7.5, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0, 22.5, 27.5, 32.5, 37.5, 42.5, 47.5, 55.0, 65.0, 75.0, 85.0, 95.0, 110.0, 130.0, 170.0, 230.]
ratedxybins = [-300,-200,-150,-100,-80,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,80,100,150,200,300]
plots = {

    'hPDF_allvall' : {'template'   : 'ROOT.TH1F("hPDFAVA_[DATASET]","",100,0,10)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'gen.hwQual() >= 12',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'getPDF(gen)*1./getPDF(rec)*1.',
                       #Plotting thingies
                       'xlabel'     : 'PDF^{Disp}/PDF^{Reco}',
                       'ylabel'     : 'L1 Muons (norm)',
                       'xrange'     : [0,10],
                       'yrange'     : [0,1.],
                       'legend'     : [0.45,0.6,0.85,0.85],
                       'legendtitle': "All L1 displaced",
                       'savename'   : '[PDIR]/hPDF_PvD',
   },
}

plots["dxy200vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy200vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY200VA")
plots["dxy200vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 200"
plots["dxy200vall"]['savename'] = '[PDIR]/hPDF_dxy200vall'
plots["dxy200vall"]['legendtitle'] = "Displaced: |d_{xy}| >= 200 cm"

plots["dxy150vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy150vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY150VA")
plots["dxy150vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 150 and  abs(getDisplacementFromPtHW(gen)) < 200"
plots["dxy150vall"]['savename'] = '[PDIR]/hPDF_dxy150vall'
plots["dxy150vall"]['legendtitle'] = "Displaced: 150 < |d_{xy}| < 200 cm"

plots["dxy100vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy100vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY100VA")
plots["dxy100vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 100 and abs(getDisplacementFromPtHW(gen)) < 150"
plots["dxy100vall"]['savename'] = '[PDIR]/hPDF_dxy100vall'
plots["dxy100vall"]['legendtitle'] = "Displaced: 100 < |d_{xy}| < 150 cm"

plots["dxy80vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy80vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY80VA")
plots["dxy80vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 80 and abs(getDisplacementFromPtHW(gen)) < 100"
plots["dxy80vall"]['savename'] = '[PDIR]/hPDF_dxy80vall'
plots["dxy80vall"]['legendtitle'] = "Displaced: 80 < |d_{xy}| < 100 cm"

plots["dxy60vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy60vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY60VA")
plots["dxy60vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 60 and abs(getDisplacementFromPtHW(gen)) < 80"
plots["dxy60vall"]['savename'] = '[PDIR]/hPDF_dxy60vall'
plots["dxy60vall"]['legendtitle'] = "Displaced: 60 < |d_{xy}| < 80 cm"

plots["dxy50vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy50vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY50VA")
plots["dxy50vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 50 and abs(getDisplacementFromPtHW(gen)) < 60"
plots["dxy50vall"]['savename'] = '[PDIR]/hPDF_dxy50vall'
plots["dxy50vall"]['legendtitle'] = "Displaced: 50 < |d_{xy}| < 60 cm"

plots["dxy40vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy40vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY40VA")
plots["dxy40vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 40 and abs(getDisplacementFromPtHW(gen)) < 50"
plots["dxy40vall"]['savename'] = '[PDIR]/hPDF_dxy40vall'
plots["dxy40vall"]['legendtitle'] = "Displaced: 40 < |d_{xy}| < 50 cm"

plots["dxy30vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy30vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY30VA")
plots["dxy30vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 30 and abs(getDisplacementFromPtHW(gen)) < 40"
plots["dxy30vall"]['savename'] = '[PDIR]/hPDF_dxy30vall'
plots["dxy30vall"]['legendtitle'] = "Displaced: 30 < |d_{xy}| < 40 cm"

plots["dxy20vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy20vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY20VA")
plots["dxy20vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 20 and abs(getDisplacementFromPtHW(gen)) < 30"
plots["dxy20vall"]['savename'] = '[PDIR]/hPDF_dxy20vall'
plots["dxy20vall"]['legendtitle'] = "Displaced: 20 < |d_{xy}| < 30 cm"

plots["dxy10vall"] = copy.deepcopy(plots['hPDF_allvall'])
plots["dxy10vall"]['template'] = plots['hPDF_allvall']['template'].replace("AVA","DXY10VA")
plots["dxy10vall"]['gencuts']  = plots['hPDF_allvall']['gencuts'] + " and " + "abs(getDisplacementFromPtHW(gen)) >= 10 and abs(getDisplacementFromPtHW(gen)) < 20"
plots["dxy10vall"]['savename'] = '[PDIR]/hPDF_dxy10vall'
plots["dxy10vall"]['legendtitle'] = "Displaced: 10 < |d_{xy}| < 20 cm"

