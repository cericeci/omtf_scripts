import ROOT
import pickle
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg 
from math import cos,sin

dump = pickle.load(open("test.pickle","r"))

img = mpimg.imread("cmsdtxy.png")
plt.imshow(img,extent=[-800.,800.,-770,770], interpolation='none')
#plt.imshow(img,extent=[0,1200.,0.,-800.], interpolation='none')
#plt.imshow(img,extent=[0,-1200.,0.,-800.], interpolation='none')

for j in range(2000):
  print "Plot:", j
  i = j*2
  plt.axis([-800,800,-770,770])
  plt.xlabel("x [cm]")
  plt.ylabel("y [cm]")
  print dump[i]["matched"]
  for j, R in enumerate(dump[i]["Rtrack"]):
    if R == 500:
      etaat500 = dump[i]["Reta"][j]

  if abs(etaat500) >= 0.82 and abs(etaat500) <= 1.24:
    if not(dump[i]["matched"]):
      plt.plot(dump[i]["vx"], dump[i]["vy"],"rx", linewidth=1)
      #plt.plot([dump[i]["Rtrack"][j]*cos(dump[i]["Rphi"][j]) for j in range(len(dump[i]["Rtrack"]))], [dump[i]["Rtrack"][j]*sin(dump[i]["Rphi"][j]) for j in range(len(dump[i]["Rtrack"]))],"r--", linewidth=3)
    else:
      plt.plot(dump[i]["vx"], dump[i]["vy"],"gx", linewidth=1)
      #plt.plot([dump[i]["Rtrack"][j]*cos(dump[i]["Rphi"][j]) for j in range(len(dump[i]["Rtrack"]))], [dump[i]["Rtrack"][j]*sin(dump[i]["Rphi"][j]) for j in range(len(dump[i]["Rtrack"]))],"g--", linewidth=3)

plt.savefig("/nfs/fanae/user/carlosec/www/testcmsdxy.pdf")
