import ROOT

rateptbins = [0.5, 1.5, 2.5, 3.5, 4.25, 4.75, 5.5, 6.5, 7.5, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0, 22.5, 27.5, 32.5, 37.5, 42.5, 47.5, 55.0, 65.0, 75.0, 85.0, 95.0, 110.0, 130.0, 170.0, 230.]
ratedxybins = [-300,-200,-150,-100,-80,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,80,100,150,200,300]
ratedxybins2 = [-300,-250,-200,-180,-160,-140,-120,-100,-80,-60,-30,0,30,60,80,100,120,140,160,180,200,250,300]
plots = {
    'rateL1Dxyv2' : {'template'   : 'ROOT.TH1F("hL1dxyv2_q12_[DATASET]","hL1dxyv2_q12_[DATASET]",21,array(\'d\',plotsfile.ratedxybins2))',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': '',
                       #Plotting thingies
                       'xlabel'     : 'L1 d_{xy} [cm]',
                       'ylabel'     : 'Rate [kHz]',
                       'norm'       : 2760*11.246,
                       'xrange'     : [-300,300],
                       'yrange'     : [0.01,1000.],
                       'legend'     : [0.5,0.2,0.9,0.4],
                       'BinCuts'    : ['getDisplacementFromPtHW(rec)==-251','getDisplacementFromPtHW(rec)==-201','getDisplacementFromPtHW(rec)==-181','getDisplacementFromPtHW(rec)==-161','getDisplacementFromPtHW(rec)==-141','getDisplacementFromPtHW(rec)==-121','getDisplacementFromPtHW(rec)==-101','getDisplacementFromPtHW(rec)==-81','getDisplacementFromPtHW(rec)==-61','getDisplacementFromPtHW(rec)==-31','getDisplacementFromPtHW(rec)==0','getDisplacementFromPtHW(rec)==31','getDisplacementFromPtHW(rec)==61','getDisplacementFromPtHW(rec)==81','getDisplacementFromPtHW(rec)==101','getDisplacementFromPtHW(rec)==121','getDisplacementFromPtHW(rec)==141','getDisplacementFromPtHW(rec)==161','getDisplacementFromPtHW(rec)==181','getDisplacementFromPtHW(rec)==201','getDisplacementFromPtHW(rec)==251'],
                       'savename'   : '[PDIR]/L1Rate_dxyv2',
   },

    'rateL1Pt' : {'template'   : 'ROOT.TH1F("hL1Pt_q12_[DATASET]","hL1Pt_q12_[DATASET]",29,array(\'d\', plotsfile.rateptbins))',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12 and rec.hwPt() <= 1000',
                       'thevariable': '1',
                       #Plotting thingies
                       'xlabel'     : 'L1 p_{T} cut [GeV]',
                       'ylabel'     : 'Rate [kHz]',
                       'norm'       : 2760*11.246,
                       'xrange'     : [0,100],
                       'yrange'     : [0,1000.],
                       'legend'     : [0.5,0.6,0.9,0.8],
                       'BinCuts'    : ['recp4.Pt()>=0','recp4.Pt()>=1','recp4.Pt()>=2','recp4.Pt()>=3','recp4.Pt()>=4','recp4.Pt()>=4.5','recp4.Pt()>=5','recp4.Pt()>=6','recp4.Pt()>=7','recp4.Pt()>=8','recp4.Pt()>=10','recp4.Pt()>=12','recp4.Pt()>=14','recp4.Pt()>=16','recp4.Pt()>=18','recp4.Pt()>=20','recp4.Pt()>=25','recp4.Pt()>=30','recp4.Pt()>=35','recp4.Pt()>=40','recp4.Pt()>=45','recp4.Pt()>=50','recp4.Pt()>=60','recp4.Pt()>=70','recp4.Pt()>=80','recp4.Pt()>=90','recp4.Pt()>=120','recp4.Pt()>=140','recp4.Pt()>=200'],
                       'savename'   : '[PDIR]/L1Rate_Pt',
   },
    'rateSplit' : {'template'   : 'ROOT.TH1F("hL1Split_q12_[DATASET]","hL1Split_q12_[DATASET]",2,-0.5,1.5)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': '1',
                       #Plotting thingies
                       'xlabel'     : 'L1 candidate is displaced',
                       'ylabel'     : 'Rate [kHz]',
                       'norm'       : 2760*11.246,
                       'xrange'     : [0.,2.],
                       'yrange'     : [10,10000.],
                       'legend'     : [0.15,0.15,0.3,0.3],
                       'BinCuts'    : ['recp4.Pt() <= 1000','recp4.Pt()>=1000'], 
                       'savename'   : '[PDIR]/L1Rate_Split',
   },

    'rateL1Dxy' : {'template'   : 'ROOT.TH1F("hL1dxy_q12_[DATASET]","hL1dxy_q12_[DATASET]",21,array(\'d\',plotsfile.ratedxybins))',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': '',
                       #Plotting thingies
                       'xlabel'     : 'L1 d_{xy} [cm]',
                       'ylabel'     : 'Rate [kHz]',
                       'norm'       : 2760*11.246,
                       'xrange'     : [-300,300],
                       'yrange'     : [0.01,100.],
                       'legend'     : [0.5,0.2,0.9,0.4],
                       'BinCuts'    : ['getDisplacementFromPtHW(rec)==-201','getDisplacementFromPtHW(rec)==-151','getDisplacementFromPtHW(rec)==-101','getDisplacementFromPtHW(rec)==-81','getDisplacementFromPtHW(rec)==-61','getDisplacementFromPtHW(rec)==-51','getDisplacementFromPtHW(rec)==-41','getDisplacementFromPtHW(rec)==-31','getDisplacementFromPtHW(rec)==-21','getDisplacementFromPtHW(rec)==-11','getDisplacementFromPtHW(rec)==0','getDisplacementFromPtHW(rec)==11','getDisplacementFromPtHW(rec)==21','getDisplacementFromPtHW(rec)==31','getDisplacementFromPtHW(rec)==41','getDisplacementFromPtHW(rec)==51','getDisplacementFromPtHW(rec)==61','getDisplacementFromPtHW(rec)==81','getDisplacementFromPtHW(rec)==101','getDisplacementFromPtHW(rec)==151','getDisplacementFromPtHW(rec)==201'],
                       'savename'   : '[PDIR]/L1Rate_dxy',
   },


    'yieldshL1Pt' : {'template'   : 'ROOT.TH1F("hL1Pt_q12_[DATASET]","hL1Pt_q12_[DATASET]",35,0,100)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'recp4.Pt()',
                       #Plotting thingies
                       'xlabel'     : 'L1 p_{T} [GeV]',
                       'ylabel'     : 'Yields',
                       'xrange'     : [0,100],
                       'yrange'     : [0,1500.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/L1_Pt_byType',
   },
    'yieldshL1Eta' : {'template'   : 'ROOT.TH1F("hL1Eta_q12_[DATASET]","hL1Eta_q12_[DATASET]",35,-2.4,2.4)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'recp4.Eta()',
                       #Plotting thingies
                       'xlabel'     : 'L1 #eta',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-2.4,2.4],
                       'yrange'     : [0,15000.],
                       'legend'     : [0.4,0.6,0.6,0.8],
                       'savename'   : '[PDIR]/L1_Eta_byType',
   },
    'yieldshL1Phi' : {'template'   : 'ROOT.TH1F("hL1Phi_q12_[DATASET]","hL1Phi_q12_[DATASET]",35,-3.14,3.14)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'recp4.Phi()',
                       #Plotting thingies
                       'xlabel'     : 'L1 #phi',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-3.14,3.14],
                       'yrange'     : [0,3000.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/L1_Phi_byType',
   },

    'yieldshL1Dxy' : {'template'   : 'ROOT.TH1F("hL1dxy_q12_[DATASET]","hL1dxy_q12_[DATASET]",61,-305,305)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'getDisplacementFromPtHW(rec)',
                       #Plotting thingies
                       'xlabel'     : 'L1 d_{xy} [cm]',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-300,300],
                       'yrange'     : [0,13000.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/L1_dxy_byType',
   },

    'yieldshL1nLayers' : {'template'   : 'ROOT.TH1F("hL1nL_q12_[DATASET]","hL1nL_q12_[DATASET]",18,-0.5,17.5)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'getNFiredLayers(rec)',
                       #Plotting thingies
                       'xlabel'     : 'L1 N_{fired layers}',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-0.5,17.5],
                       'yrange'     : [0,13000.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/L1_nLayers_byType',
   },
    'yieldsL1Layers' : {'template'   : 'ROOT.TH1F("hL1L_q12_[DATASET]","hL1L_q12_[DATASET]",20,-0.5,19.5)',
                       'PlotGen'    : False,
                       'PlotReco'   : True,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '',
                       #These are applied in the effficiency numerator
                       'recocuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'getFiredLayers(rec)',
                       #Plotting thingies
                       'xlabel'     : 'L1 Layer',
                       'ylabel'     : 'Hits',
                       'xrange'     : [-0.5,19.5],
                       'yrange'     : [0,100000.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/L1_Layers_byType',
                       'xlabels'    : ["MB1","MB1b","MB2","MB2b","MB3","MB3b","ME1/3","ME2/3","ME3/3","ME1/2","RB1in","RB1out","RB2in","RB2out","RB3","RE1/3","RE2/3","RE3/3"],
   },
    'yieldsGENPt' : {'template'   : 'ROOT.TH1F("hGENPt_q12_[DATASET]","hL1Pt_q12_[DATASET]",35,0,100)',
                       'PlotGen'    : True,
                       'PlotReco'   : False,
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'recocuts'   : '1',
                       'thevariable': 'genp4.Pt()',
                       #Plotting thingies
                       'xlabel'     : 'Gen p_{T} [GeV]',
                       'ylabel'     : 'Yields',
                       'xrange'     : [0,100],
                       'yrange'     : [0,7500.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/GEN_Pt',
   },
    'yieldsGENEta' : {'template'   : 'ROOT.TH1F("hGENEta_q12_[DATASET]","hL1Pt_q12_[DATASET]",100,-5.0,5.0)',
                       'PlotGen'    : True,
                       'PlotReco'   : False,
                       #These are applied in the efficiency denominator
                       'gencuts'    : '1',
                       #These are applied in the effficiency numerator
                       'recocuts'   : '1',
                       'thevariable': 'genp4.Eta()',
                       #Plotting thingies
                       'xlabel'     : 'Gen #eta',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-5.,5.],
                       'yrange'     : [0,22500.],
                       'legend'     : [0.3,0.6,0.7,0.8],
                       'savename'   : '[PDIR]/GEN_Eta',
   },
    'yieldsGENPhi' : {'template'   : 'ROOT.TH1F("hGENPhi_q12_[DATASET]","hL1Pt_q12_[DATASET]",62,-3.1,3.1)',
                       'PlotGen'    : True,
                       'PlotReco'   : False,
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'recocuts'   : '1',
                       'thevariable': 'genp4.Phi()',
                       #Plotting thingies
                       'xlabel'     : 'Gen #phi',
                       'ylabel'     : 'Yields',
                       'xrange'     : [-3.2,3.2],
                       'yrange'     : [0,3500.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/GEN_Phi',
   },
    'yieldsGENDxy' : {'template'   : 'ROOT.TH1F("hGENDxy_q12_[DATASET]","hL1DXY_q12_[DATASET]",30,0,300)',
                       'PlotGen'    : True,
                       'PlotReco'   : False,
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'recocuts'   : '',
                       'thevariable': 'abs((-1*gen.vx()* genp4.Py() + gen.vy()* genp4.Px()) / genp4.Pt())',
                       #Plotting thingies
                       'xlabel'     : 'Gen |d_{xy}| [cm]',
                       'ylabel'     : 'Yields',
                       'xrange'     : [0,300],
                       'yrange'     : [0,6500.],
                       'legend'     : [0.6,0.6,0.9,0.8],
                       'savename'   : '[PDIR]/GEN_Dxy',
   },

    'EffPt' : {'template'   : 'ROOT.TEfficiency("hEffPt_ptgen0_q12_[DATASET]","",35,30,100)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'genp4.Pt()',
                       #Plotting thingies
                       'xlabel'     : 'Gen p_{T} [GeV]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [-60,100],
                       'yrange'     : [0,1.],
                       'legend'     : [0.1,0.1,0.55,0.5],
                       'savename'   : '[PDIR]/hEff_Pt_byType',
   },
   'EffDxy' : {'template'   : 'ROOT.TEfficiency("hEffDxy_ptgen0_q12_[DATASET]","",48,0,240)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 12',
                       'thevariable': 'abs((-1*gen.vx()* genp4.Py() + gen.vy()* genp4.Px()) / genp4.Pt())',
                       #Plotting thingies
                       'xlabel'     : 'Gen d_{xy} [cm]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,240],
                       'yrange'     : [0,1.4],
                       'legend'     : [0.35,0.65,0.90,1.],
                       'savename'   : '[PDIR]/hEff_dxy_byType',
   },
   'EffEta' : {'template'   : 'ROOT.TEfficiency("hEffEta_ptgen0_q12_[DATASET]","",120,0.,2.4)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : '1',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 12',
                       'thevariable': 'abs(genp4.Eta())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #eta',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,2.4],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.55,0.15,1.0,0.75],
                       'savename'   : '[PDIR]/hEff_Eta_byType',
   },
   'EffPhi' : {'template'   : 'ROOT.TEfficiency("hEffPhi_ptgen0_q12_[DATASET]","",60,-3.14,3.14)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 12',
                       'thevariable': '(genp4.Phi())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #phi',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [-3.14,10.14],
                       'yrange'     : [0,1.],
                       'legend'     : [0.5,0.15,1.0,0.75],
                       'savename'   : '[PDIR]/hEff_Phi_byType',
   },

    'EffPtq4' : {'template'   : 'ROOT.TEfficiency("hEffPt_ptgen0_q4_[DATASET]","",35,30,100)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 4',
                       'thevariable': 'genp4.Pt()',
                       #Plotting thingies
                       'xlabel'     : 'Gen p_{T} [GeV]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,100],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.6,0.6,0.9,0.99],
                       'savename'   : '[PDIR]/hEff_Pt_byTypeq4',
   },
   'EffDxyq4' : {'template'   : 'ROOT.TEfficiency("hEffDxy_ptgen0_q4_[DATASET]","",48,0,240)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 4',
                       'thevariable': 'abs((-1*gen.vx()* genp4.Py() + gen.vy()* genp4.Px()) / genp4.Pt())',
                       #Plotting thingies
                       'xlabel'     : 'Gen d_{xy} [cm]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,240],
                       'yrange'     : [0,1.],
                       'legend'     : [0.1,0.1,0.3,0.5],
                       'savename'   : '[PDIR]/hEff_dxy_byTypeq4',
   },
   'EffEtaq4' : {'template'   : 'ROOT.TEfficiency("hEffEta_ptgen0_q4_[DATASET]","",120,0.,2.4)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : '1',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 4',
                       'thevariable': 'abs(genp4.Eta())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #eta',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,2.4],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.6,0.15,0.9,0.75],
                       'savename'   : '[PDIR]/hEff_Eta_byTypeq4',
   },
   'EffPhiq4' : {'template'   : 'ROOT.TEfficiency("hEffPhi_ptgen0_q4_[DATASET]","",60,-3.14,3.14)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 4',
                       'thevariable': '(genp4.Phi())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #phi',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [-3.14,7.14],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.6,0.15,0.9,0.75],
                       'savename'   : '[PDIR]/hEff_Phi_byTypea4',
   },

    'EffPtq0' : {'template'   : 'ROOT.TEfficiency("hEffPt_ptgen0_q0_[DATASET]","",35,30,100)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 0',
                       'thevariable': 'genp4.Pt()',
                       #Plotting thingies
                       'xlabel'     : 'Gen p_{T} [GeV]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,100],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.6,0.6,0.9,0.99],
                       'savename'   : '[PDIR]/hEff_Pt_byTypeq0',
   },
   'EffDxyq0' : {'template'   : 'ROOT.TEfficiency("hEffDxy_ptgen0_q0_[DATASET]","",48,0,240)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'   : 'rec.hwQual() >= 0',
                       'thevariable': 'abs((-1*gen.vx()* genp4.Py() + gen.vy()* genp4.Px()) / genp4.Pt())',
                       #Plotting thingies
                       'xlabel'     : 'Gen d_{xy} [cm]',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,240],
                       'yrange'     : [0,1.],
                       'legend'     : [0.1,0.1,0.3,0.5],
                       'savename'   : '[PDIR]/hEff_dxy_byTypeq0',
   },
   'EffEtaq0' : {'template'   : 'ROOT.TEfficiency("hEffEta_ptgen0_q10_[DATASET]","",120,0.,2.4)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : '1',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 0',
                       'thevariable': 'abs(genp4.Eta())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #eta',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [0,2.4],
                       'yrange'     : [0,1.5],
                       'legend'     : [0.6,0.15,0.9,0.75],
                       'savename'   : '[PDIR]/hEff_Eta_byTypeq0',
   },
   'EffPhiq0' : {'template'   : 'ROOT.TEfficiency("hEffPhi_ptgen0_q0_[DATASET]","",60,-3.14,3.14)',
                       #These are applied in the efficiency denominator
                       'gencuts'    : 'abs(genp4.Eta()) >= 0.82 and abs(genp4.Eta()) <= 1.24',
                       #These are applied in the effficiency numerator
                       'numcuts'    : 'rec.hwQual() >= 0',
                       'thevariable': '(genp4.Phi())',
                       #Plotting thingies
                       'xlabel'     : 'Gen #phi',
                       'ylabel'     : 'Efficiency',
                       'xrange'     : [-3.14,7.14],
                       'yrange'     : [0,1.],
                       'legend'     : [0.6,0.15,0.9,0.75],
                       'savename'   : '[PDIR]/hEff_Phi_byTypeq0',
   },

}

