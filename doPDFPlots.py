import ROOT
import os

ROOT.gROOT.SetBatch(True)
tf = ROOT.TFile("PatternsDisplaced_0x0005_2_optimized.root")

td = "patternsPdfs/canvases"

nPats = 22
for i in range(nPats):
  print i
  tc = tf.Get(td+"/PatNum_"+str(i))
  print td+"/PatNum_"+str(i)
  tc.Draw()
  tc.SaveAs("~/www/patterns_displaced_newbins_minus/pattern%i.png"%i)

