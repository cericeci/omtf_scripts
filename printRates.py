import ROOT
from DataFormats.FWLite import Events, Handle
import operator
from auxiliars import * 

events = Events("l1tomtf_superprimitives1_rate_patterns0x0011.root")
muonHandle, muonLabel = Handle("BXVector<l1t::RegionalMuonCand>"), ("simBayesOmtfDigisBoth", "OMTF", "L1TMuonEmulation" )


confs = {}
qual = {}
count = 0
for ev in events:
  if not count%1000:  print count, events.size()
  #if count == 10: break
  count = count + 1
  ev.getByLabel(muonLabel, muonHandle)
  muons = muonHandle.product()
  for mu in muons:
    if getP4FromHW(mu).Pt() < 4000: continue
    c =  bin(mu.trackAddress()[0])[2:]
    if not(c in confs):
      confs[c]  = [1,0]
      qual[c] = mu.hwQual()
    else:
      confs[c][0] += 1

eventsEff = Events("l1tomtf_superprimitives1_withBXm4top4_patterns0x0011.root")

count = 0
for ev in eventsEff:
  if not count%1000:  print count, eventsEff.size()
  #if count == 10: break
  count = count + 1
  ev.getByLabel(muonLabel, muonHandle)
  muons = muonHandle.product()
  for mu in muons:
    if getP4FromHW(mu).Pt() < 4000 or mu.hwQual() < 12 : continue
    c =  bin(mu.trackAddress()[0])[2:]
    if not(c in confs):
      confs[c]  = [0,1]
      qual[c] = mu.hwQual()
    elif len(confs[c]) == 2:
      confs[c][1] += 1

totEff  = sum([confs[c][1] for c in confs.keys()])
totRate = 0. #sum([confs[c][1] for c in confs.keys()])*2760*11.246/events.size() 
#print totEff
#print confs
"""for c in confs.keys():
  confs[c] = [confs[c][0]/(confs[c][1]+0.000000001), confs[c][0], confs[c][1]]
"""
sorted_confs = sorted(confs.items(), key=operator.itemgetter(1))

"""partEff  = 0.
partRate = 0.
for sc in sorted_confs:
  if qual[sc[0]] != 12: continue
  partRate += sc[1][1]*2760*11.246/events.size()
  partEff  += sc[1][2]
  if partRate >= 1:
    totRate += partRate
    partRate = 0
    print "Rate is %1.3f at Eff %1.3f"%(totRate, partEff/(totEff*1.))
totRate += partRate
print "Rate is %1.3f at Eff %1.3f"%(totRate, partEff/(totEff*1.))
"""

dumpRate = 0
dumpEff  = 0
dumpP = []
for sc in sorted_confs:
  pat  = sc[0]
  rate = sc[1][0]*2760*11.246/events.size()
  eff  = sc[1][1]*1./(totEff*1.)
  while len(pat) < 18:
    pat = "0"+pat
  if qual[sc[0]] != 12: continue
  print pat, "%2.3f"%rate, "%1.5f"%eff, qual[sc[0]]
  """if rate/(eff+0.0000000000001) >= 1000 and rate >= 1 :
     print pat, "%2.3f"%rate, "%1.5f"%eff, qual[sc[0]]
     dumpRate += rate
     dumpEff  += eff
     dumpP.append(pat)"""
  totRate += rate

print totEff, totRate

print dumpP, dumpEff, dumpRate
