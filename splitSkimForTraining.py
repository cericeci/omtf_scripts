import FWCore.ParameterSet.Config as cms
process = cms.Process("MuonSplitter")

process.load("FWCore.MessageLogger.MessageLogger_cfi")

process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(1)
process.options = cms.untracked.PSet(wantSummary = cms.untracked.bool(False))

import os
#files =tuple(["file:/pool/phedexrw/userstorage/carlosec/omtf/privateSamples/lxplus/part2/"+f for f in os.listdir("/pool/phedexrw/userstorage/carlosec/omtf/privateSamples/lxplus/part2/")])
files=tuple(["file:/pool/phedexrw/userstorage/carlosec/omtf/privateSamples/lxplus/part1/merged.root"])
process.source = cms.Source('PoolSource',
 fileNames = cms.untracked.vstring(files
 ),
 duplicateCheckMode = cms.untracked.string("noDuplicateCheck"),                          
)
	                    
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1))
# PostLS1 geometry used
process.load('Configuration.Geometry.GeometryExtended2015Reco_cff')
process.load('Configuration.Geometry.GeometryExtended2015_cff')

process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.MagneticField_cff')

############################
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '103X_upgrade2023_realistic_v2', '')


####Event Setup Producer

process.out = cms.OutputModule("PoolOutputModule", 
   fileName = cms.untracked.string("part1_merged_positive.root"),
   SelectEvents = cms.untracked.PSet(SelectEvents=cms.vstring('dispPath')),
   outputCommands = cms.untracked.vstring('drop *',
    'keep *_g4SimHits_*_*',
    'keep *_simDtTriggerPrimitiveDigis_*_*',
    'keep *_simCscTriggerPrimitiveDigis_*_*',
    'keep *_simMuonRPCDigis_*_*',
    'keep *_generator*_*_*',
    'keep *_genParticles_*_*',
    'keep *_dispGenEta_*_*',
    'drop PSimHits_g4SimHits_MuonDTHits_HLT',
    'drop PSimHits_g4SimHits_MuonCSCHits_HLT',
    'drop *GEMDetId*_*_*_*',
    'drop RPCDigiSimLinkedmDetSetVector_simMuonRPCDigis_RPCDigiSimLink_HLT'),
)

process.muonFilter = cms.EDFilter("GenMuEtaAndSignFilter",
      src = cms.InputTag("genParticles"),
      muvaluemap = cms.InputTag("dispGenEta","genParticledispEta","MuonMatcher"),
      minEtaCut  = cms.double(0.75),
      maxEtaCut  = cms.double(1.3),
      sign       = cms.double(1.0),
)

process.options.numberOfThreads=cms.untracked.uint32(8)
process.options.numberOfStreams=cms.untracked.uint32(0)


process.dispPath = cms.Path(process.muonFilter)
process.output_step = cms.EndPath(process.out)
process.schedule = cms.Schedule(process.dispPath)
process.schedule.extend([process.output_step])
